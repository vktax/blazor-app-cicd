﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using BlazorApp.Data.DTOs;
using BlazorApp.Data.Entities;

namespace BlazorApp.Data
{
    public class MessageService
    {
        private readonly BlazorDbContext context;
        private readonly ILogger logger;

        public MessageService(BlazorDbContext dbContext, ILogger<MessageService> iLogger)
        {
            context = dbContext;
            logger = iLogger;
        }

        public async Task<MessageItemDTO> GetMessage()
        {
            MessageItem item = await context.Messages.FirstOrDefaultAsync();
            
            if (item == null)
            {
                logger.LogError("Message record not found.");
                return new MessageItemDTO
                {
                    Message = String.Empty
                };
            }

            return new MessageItemDTO
            {
                Message = item.Message
            };
        }
    }
}
