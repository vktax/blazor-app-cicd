# BlazorApp

## Development

* Clone the repository
* `cd` into the project folder
* Export env variable for connection string: https://azure.microsoft.com/en-us/blog/windows-azure-web-sites-how-application-strings-and-connection-strings-work/
* Launch the application: `dotnet run --project ./BlazorApp`

## Azure Portal Setup

* Create app service with desired settings
* Create Sql Server Database
* Add a new connection string for the Sql Server database created in the previous step
* Connection string key: `DefaultConnection`
* Connection string value: copy from the Azure Sql and don't forget to replace password placeholder
* After launching the application, use Azure Portal Query editor to insert a new record, e.g.:
```sql
insert into dbo.Messages (message) values ('hello')
```

## Code Deployment with VS Code

* Source: https://docs.microsoft.com/en-us/aspnet/core/tutorials/publish-to-azure-webapp-using-vscode?view=aspnetcore-3.1
* `cd` into the repository folder: `./BlazorApp`
* Publish the application locally: `dotnet publish ./BlazorApp -c Release -o ./publish`
* Deploy `publish` folder to the Azure App Service

## Azure DevOps Pipeline

### Create Service Connection

* Navigate to Azure DevOps
* Select `Project Settings` -> `Service Connections`
* Select `Create Service Connection` -> `Azure Resource Manager`
* Select `Next`, `Service Principal`, and `Next`
* Select a subscription and a resource group
* Provide `Service Connection Name` and save your changes

### Create New Pipeline

* Select `Azure Repos Git (yaml)`
* Select this repository
* Select `Select an existing YAML file` option
* Select path to `cicd-pipeline.yaml` file

